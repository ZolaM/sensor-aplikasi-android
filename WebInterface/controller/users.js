var express = require('express');
var router = express.Router();
var http= require('http');
var io = require('../socket/socket.js')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/login', function(req, res, next) {
  res.send('login page');
});
router.get('/kirim', function(req, res, next) {
	var string = req.query.status;
	io.sendEvent('update', string, globalIO)
  	res.send(string);
});
router.get('/update', function(req, res, next) {
	//var string = req.query.status;
  	res.render('client', {title: 'update status'});
});

module.exports = router;
