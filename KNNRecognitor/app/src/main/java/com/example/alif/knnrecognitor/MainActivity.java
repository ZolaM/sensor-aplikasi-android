package com.example.alif.knnrecognitor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import weka.classifiers.Classifier;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView status;
    private TextView currentX, currentY, currentZ;
    private float X, Y, Z, SumX,SumY,SumZ, avgX, avgY, avgZ;
    private SensorManager sensorManager;
    private Sensor accelerometer, proximity;
    Instances data,dataSet;
    int counter;
    Classifier ibk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        SumX = SumY = SumZ = avgX = avgY = avgZ = counter = 0;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer

            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            // fai! we dont have an accelerometer!
        }

        proximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        BufferedReader datafile = readDataFile("/sdcard/training.arff");

        Instances data = null;
        try {
            data = new Instances(datafile);
            data.setClassIndex(data.numAttributes()-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        data.setClassIndex(data.numAttributes() - 1);

        ibk = new IBk();
        try {
            ibk.buildClassifier(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        status = (TextView) findViewById(R.id.konsisi_value);

    }

    public static BufferedReader readDataFile(String filename) {
        BufferedReader inputReader = null;

        try {
            inputReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }

        return inputReader;
    }

    public void initializeViews() {
        currentX = (TextView) findViewById(R.id.currentX);
        currentY = (TextView) findViewById(R.id.currentY);
        currentZ = (TextView) findViewById(R.id.currentZ);
    }

    public void displayCurrentValues() {
        currentX.setText(Float.toString(X));
        currentY.setText(Float.toString(Y));
        currentZ.setText(Float.toString(Z));
    }

    public void makeDataSet()
    {
        FastVector atts;
        FastVector kelas;

        double[] vals;

        // 1. set up attributes
        atts = new FastVector();
        // - numeric
        atts.addElement(new Attribute("X"));
        atts.addElement(new Attribute("Y"));
        atts.addElement(new Attribute("Z"));

        kelas = new FastVector();
        kelas.addElement("Duduk");
        kelas.addElement("Berdiri");
        kelas.addElement("Jalan");
        atts.addElement(new Attribute("Class", kelas));

        dataSet = new Instances("whatever", atts, 0);
        dataSet.setClassIndex(dataSet.numAttributes() - 1);

        vals = new double[dataSet.numAttributes()];
        //Log.d("angka", Integer.toString(data.numAttributes()));
        vals[0] = avgX;
        vals[1] = avgY;
        vals[2] = avgZ;
        vals[3] = Instance.missingValue();

        dataSet.add(new Instance(1.0, vals));

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        displayCurrentValues();

        X = sensorEvent.values[0];
        Y = sensorEvent.values[1];
        Z = sensorEvent.values[2];

        if(counter < 20)
        {
            Log.d("iterasi", Integer.toString(counter));
            SumX = SumX + X;
            SumY = SumY + Y;
            SumZ = SumZ + Z;
            counter++;

            if(counter==19)
            {
                avgX = (float) (SumX / 20.0);
                avgY = (float) (SumY / 20.0);
                avgZ = (float) (SumZ / 20.0);

                makeDataSet();
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try
                {
                    String kondisi;
                    Double kelas = ibk.classifyInstance(dataSet.firstInstance());
                    if(kelas==0.0) kondisi = "Duduk";
                    else if(kelas==1.0) kondisi = "Berdiri";
                    else kondisi = "Jalan";
                    status.setText(kondisi);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                SumX = SumY = SumZ = avgX = avgY = avgZ = counter = 0;
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
